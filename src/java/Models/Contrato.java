/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.MySQL;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author LaboratorioFISI
 */
public class Contrato {
    private int idContrato;
    private Filial original;
    private float sueldobase;

    public Contrato(Trabajador trabajador) throws SQLException {
        MySQL cnx = new MySQL();        
        ResultSet resultSet = cnx.getStatement().executeQuery("call getContrato("+trabajador.getDni()+")");
        while (resultSet.next()) {
            this.idContrato = resultSet.getInt(1);
            this.sueldobase = resultSet.getFloat(2);
            this.original = new Filial(resultSet.getInt(4));            
        }
        cnx.closeConexion();
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public Filial getOriginal() {
        return original;
    }

    public void setOriginal(Filial original) {
        this.original = original;
    }

    public float getSueldobase() {
        return sueldobase;
    }

    public void setSueldobase(float sueldobase) {
        this.sueldobase = sueldobase;
    }

}
