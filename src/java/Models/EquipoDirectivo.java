/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author LaboratorioFISI
 */
class EquipoDirectivo {
    private ArrayList<Directivo> directivos;
    private Presidente presidente;

    public EquipoDirectivo(ArrayList<Directivo> directivos, Presidente presidente) {
        this.directivos = directivos;
        this.presidente = presidente;
    }

    public ArrayList<Directivo> getDirectivos() {
        return directivos;
    }

    public void setDirectivos(ArrayList<Directivo> directivos) {
        this.directivos = directivos;
    }

    public Presidente getPresidente() {
        return presidente;
    }

    public void setPresidente(Presidente presidente) {
        this.presidente = presidente;
    }
}
