/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.MySQL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author LaboratorioFISI
 */
public class ListaTrabajadores {
    private ArrayList<Trabajador> trabajadores;

    public ListaTrabajadores() throws SQLException {
        trabajadores = new ArrayList<>();
        MySQL cnx = new MySQL();
        ResultSet resultSet = cnx.getConexion().createStatement().executeQuery("call getTrabajadores()");
        while (resultSet.next()) {
            this.trabajadores.add(new Trabajador(resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getString(4)));
        }
        cnx.closeConexion();
    }
    
    public Trabajador buscarTrabajador(int dni){
        for(int x=0; x<trabajadores.size(); x++){
            if(trabajadores.get(x).getDni() == dni){
                return trabajadores.get(x);
            }
        }
        return null;
    }

    public ArrayList<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(ArrayList<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }
    
    
}
