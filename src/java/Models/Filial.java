/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.MySQL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author LaboratorioFISI
 */
public class Filial {
    private int idFilial;
    private String nombre;
    private String tipoFilial;
    private int nroTrabajadoresBase;
    private EquipoDirectivo directivos;
    private ArrayList<Trabajador> trabajadores;

    public Filial(int idFilial) throws SQLException {
        MySQL cnx = new MySQL();
        ResultSet resultSet = cnx.getStatement().executeQuery("call getFilial(" + idFilial + ")");
        while (resultSet.next()) {
            this.idFilial = resultSet.getInt(1);
            this.nombre = resultSet.getString(2);
            this.tipoFilial = resultSet.getString(3);
            this.nroTrabajadoresBase = resultSet.getInt(4);
        }
        cnx.closeConexion();
        
        this.directivos = null;
        this.trabajadores = null;
    }

    public int getIdFilial() {
        return idFilial;
    }

    public void setIdFilial(int idFilial) {
        this.idFilial = idFilial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoFilial() {
        return tipoFilial;
    }

    public void setTipoFilial(String tipoFilial) {
        this.tipoFilial = tipoFilial;
    }

    public int getNroTrabajadoresBase() {
        return nroTrabajadoresBase;
    }

    public void setNroTrabajadoresBase(int nroTrabajadoresBase) {
        this.nroTrabajadoresBase = nroTrabajadoresBase;
    }

    public EquipoDirectivo getDirectivos() {
        return directivos;
    }

    public void setDirectivos(EquipoDirectivo directivos) {
        this.directivos = directivos;
    }

    public ArrayList<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(ArrayList<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }
    
    
}
