/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.SQLException;

/**
 *
 * @author LaboratorioFISI
 */
public class Trabajador extends Persona{
    private Contrato contrato;

    public Trabajador(int dni, String nombre, int edad, String direccion) throws SQLException {
        super(dni, nombre, edad, direccion);
        contrato = new Contrato(this);
    }
    
    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    
}
