/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author LaboratorioFISI
 */
public class Informe {
    private int anio;
    private ArrayList<BloqueMensual> bloques;

    public Informe(Trabajador trabajador, int anio) throws SQLException {
        this.anio = anio;
        bloques = new ArrayList<>();
        
        for(int mes=1; mes<=12; mes++)
            bloques.add(new BloqueMensual(trabajador, anio, mes));
    }
    
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public ArrayList<BloqueMensual> getBloque() {
        return bloques;
    }

    public void setBloque(ArrayList<BloqueMensual> bloque) {
        this.bloques = bloque;
    }
    
}
