/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Utils.MySQL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author LaboratorioFISI
 */
public class BloqueMensual {

    private ArrayList<Filial> filial;
    private ArrayList<Boolean> original;
    private ArrayList<Float> horas;
    private ArrayList<Float> sueldoxfilial;
    private float sueldoFinal;

    public ArrayList<Float> getSueldoxfilial() {
        return sueldoxfilial;
    }

    public void setSueldoxfilial(ArrayList<Float> sueldoxfilial) {
        this.sueldoxfilial = sueldoxfilial;
    }
    

    public BloqueMensual(Trabajador trabajador, int anio, int mes) throws SQLException {
        filial = new ArrayList<>();
        original = new ArrayList<>();
        horas = new ArrayList<>();
        sueldoxfilial = new ArrayList<>();
        sueldoFinal = 0;

        MySQL cnx = new MySQL();
        String variables = trabajador.getDni() + "," + anio + "," + mes;
        ResultSet resultSet = cnx.getStatement().executeQuery("call getInforme(" + variables + ")");

        while (resultSet.next()) {
            filial.add(new Filial(resultSet.getInt(1)));
            if (resultSet.getInt(2) == 1) {
                original.add(true);
            } else {
                original.add(false);
            }
            horas.add(resultSet.getFloat(3));
        }

        cnx.closeConexion();

        this.calcularSueldo(trabajador);
    }

    private void calcularSueldo(Trabajador trabajador) {
        for (int x = 0; x < filial.size(); x++) {
            sueldoFinal += horas.get(x) * trabajador.getContrato().getSueldobase();
            sueldoxfilial.add(horas.get(x) * trabajador.getContrato().getSueldobase());
        }
    }

    public ArrayList<Filial> getFilial() {
        return filial;
    }

    public void setFilial(ArrayList<Filial> filial) {
        this.filial = filial;
    }

    public ArrayList<Boolean> getOriginal() {
        return original;
    }

    public void setOriginal(ArrayList<Boolean> original) {
        this.original = original;
    }

    public ArrayList<Float> getHoras() {
        return horas;
    }

    public void setHoras(ArrayList<Float> horas) {
        this.horas = horas;
    }

    public float getSueldofinal() {
        return sueldoFinal;
    }

    public void setSueldofinal(float sueldofinal) {
        this.sueldoFinal = sueldofinal;
    }

}
