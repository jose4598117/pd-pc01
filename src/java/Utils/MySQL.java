package Utils;


import java.sql.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author José La Rosa
 */
public class MySQL {

    private Connection conexion = null;
    private Statement statement = null;
        

    public MySQL() {
        String database = "mydb";
        String username = "root";
        String password = "";
        try {
            System.out.println("Iniciando");
            Class.forName("com.mysql.jdbc.Driver");
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String url = "jdbc:mysql://localhost:3307/" + database;
            conexion = DriverManager.getConnection(url, username, password);
            System.out.println("Conexion a Base de Datos " + url + " . . . . .Ok");
            statement = conexion.createStatement();
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }
    
    public void closeConexion() throws SQLException {
        conexion.close();
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
}
