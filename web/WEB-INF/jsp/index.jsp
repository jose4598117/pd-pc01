<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" import="Models.*" %>

<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Zapatería - Informe de Personal</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <style>
            @import "https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700";
            html, body {
                height: 100%;
                margin: 0;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background: linear-gradient(#26A4F7, #A6DCFF);
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 110vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {

                font-family: 'Oswald', sans-serif;
                font-size: 70px;
                border: thin;
                border-radius: 10px;
                -webkit-background-clip: text;
                -moz-background-clip: text;
                background-clip: text;
                color: white;
                text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
            }

            .subtitle {
                height: 50%;
                font-family: 'Oswald', sans-serif;
                font-size: 40px;
                border: thin;
                border-radius: 10px;
                -webkit-background-clip: text;
                -moz-background-clip: text;
                background-clip: text;
                color: white;
                text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
            }

            .links > a {
                color: #000000;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .line {
                width: 100%;
                height: 1px;
                border-bottom: 1px dashed #ddd;
                margin: 40px 0;
            }

            #customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 80%;
            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}
            #customers tr:nth-child(odd){background-color: #FFFFFF;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #1984CB;
                color: white;
            }


        </style>

    </head>

    <body>

        <div class="content">
            <div class="title m-b-md">
                <B>MultiCorp - Visualización de Informes</B>
            </div>  
            <div class="line"></div>
            <div class="subtitle m-b-md">Lista de trabajadores</div>
            <div class="container">
                <center>
                    <ul style="list-style-type:none;">
                        <table id="customers">
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Filial</th>
                            </tr>
                            <%
                                ListaTrabajadores trabajadores = new ListaTrabajadores();
                                for (int x = 0; x < trabajadores.getTrabajadores().size(); x++) {
                                    out.println("<tr>");
                                    out.println("<td>" + trabajadores.getTrabajadores().get(x).getDni() + "</td>");
                                    out.println("<td>" + trabajadores.getTrabajadores().get(x).getNombre() + "</td>");
                                    out.println("<td>" + trabajadores.getTrabajadores().get(x).getContrato().getOriginal().getNombre() + "</td>");
                                    out.println("</tr>");
                                }
                            %>
                        </table>

                    </ul> 
                </center>
            </div>
            <div class="line"></div>
            <div class="container">
                <center>
                    <form name = 'informe' method='get' action='informe'>
                        Código del trabajador :  
                        <input type="text" name="cod"><br>
                        <br>
                        <input type="submit" value="Buscar Informe">
                    </form>
                </center>
            </div>
        </div>               
    </body>
</html>


