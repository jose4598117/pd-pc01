<%-- 
    Document   : informe
    Created on : 13/04/2019, 06:22:09 PM
    Author     : José La Rosa
--%>

<%@page import="Models.*"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Trabajador trabajador = new ListaTrabajadores().buscarTrabajador(Integer.parseInt(request.getParameter("cod")));
        Informe informe = new Informe(trabajador, 2019);
    %>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informe anual de trabajador </title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <style>
            @import "https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700";
            html, body {
                height: 100%;
                margin: 0;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background: linear-gradient(#26A4F7, #A6DCFF);
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 110vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {

                font-family: 'Oswald', sans-serif;
                font-size: 70px;
                border: thin;
                border-radius: 10px;
                -webkit-background-clip: text;
                -moz-background-clip: text;
                background-clip: text;
                color: white;
                text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
            }

            .subtitle {
                height: 50%;
                font-family: 'Oswald', sans-serif;
                font-size: 40px;
                border: thin;
                border-radius: 10px;
                -webkit-background-clip: text;
                -moz-background-clip: text;
                background-clip: text;
                color: white;
                text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
            }

            .links > a {
                color: #000000;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .line {
                width: 100%;
                height: 1px;
                border-bottom: 1px dashed #ddd;
                margin: 40px 0;
            }

            #customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 80%;
            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}
            #customers tr:nth-child(odd){background-color: #FFFFFF;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #1984CB;
                color: white;
            }


        </style>
    </head>
    <body>
        <div class="content">
            <br>
            <div class="title m-b-md">
                <%
                    out.println("<h1>Informe anual de " + trabajador.getNombre() + " - " + informe.getAnio() + "</h1>");
                %>
            </div>

            <div class="line"></div>

            <div class="subtitle m-b-md">Resultado de Busqueda</div>
            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet"/>
            <div class="container">
                <center>
                    <div class="row">
                        <div class="col" align = "left">
                            <strong>Nombre del Trabajador:   </strong> <% out.print(trabajador.getNombre()); %>                                            
                        </div>
                        <div class="col">
                            <strong>Edad del Trabajador:   </strong> <% out.print(trabajador.getEdad()); %>
                        </div>
                    </div>
                    <br>
                    <div class="row" align = "left">
                        <div ><strong>Dirección del Trabajador:   </strong> <% out.print(trabajador.getDireccion()); %></div>
                    </div>       
                    <br>
                    <div class="row">
                        <div class="col" align = "left">
                            <strong>Sueldo Base por Hora:   </strong> S/  <% out.print(trabajador.getContrato().getSueldobase()); %>
                        </div>
                        <div class="col">
                            <strong>Filial original:   </strong> <% out.print(trabajador.getContrato().getOriginal().getNombre());%>
                        </div>
                    </div>
                </center>
            </div>

            <div class="line"></div>

            <div class="container">

                <div class="subtitle m-b-md">Informe Anual</div>
                <%
                    for (int x = 0; x < informe.getBloque().size(); x++) {
                        BloqueMensual bloque = informe.getBloque().get(x);
                        out.println("<div>");
                        out.println("<h2>" + new DateFormatSymbols().getMonths()[x] + "</h2>");
                        out.println("<br>");
                        out.println("<div class=\"container\">");
                        out.println("<center>");
                        out.println("<ul style=\"list-style-type:none;\">");
                        out.println("<table id=\"customers\">");
                        out.println("<tr>");
                        out.println("<th>Filial</th>");
                        out.println("<th>Tipo Filial</th>");
                        out.println("<th>Horas</th>");
                        out.println("<th>Sueldo por filial</th>");
                        out.println("</tr>");

                        for (int y = 0; y < bloque.getFilial().size(); y++) {
                            out.println("<tr>");
                            out.println("<td>" + bloque.getFilial().get(y).getNombre() + "</td>");
                            if (bloque.getOriginal().get(y)) {
                                out.println("<td> Original </td>");
                            } else {
                                out.println("<td> Horas Extra </td>");
                            }
                            out.println("<td>" + bloque.getHoras().get(y) + "</td>");
                            out.println("<td> S/ " + bloque.getSueldoxfilial().get(y) + "</td>");
                            out.println("</tr>");
                        }
                        out.println("</table>");
                        out.println("</ul>"); 
                        out.println("<h4>Sueldo Total del mes:  S/ "+bloque.getSueldofinal()+"</h4>");
                        out.println("</center>");
                        out.println("</div>");
                        out.println("</div>");
                        out.println("<br>");
                        out.println("<div class=\"line\"></div>");
                        
                    }
                %>
            </div>

        </div>

    </body>
</html>
